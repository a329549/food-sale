import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { NavigationContainer } from "@react-navigation/native";
import Home from "./screeens/Home";
import Add from './screeens/Add';

const Stack = createNativeStackNavigator();

function MyStack() {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Home" component={Home} options={{title: "Home",headerTitleAlign: "center"}}/>
            <Stack.Screen name="Add" component={Add} options={{title: "Add a new product",presentation: 'modal'}}/>
        </Stack.Navigator>
    )
}

export default function Navegation() {
    return (
        <NavigationContainer>
            <MyStack/>
        </NavigationContainer>
    )
}