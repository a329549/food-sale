# Food sale 🌮

Es un pequeña aplicación que simula vender comida. Es mi primer aplicación multiplataforma utilizando react native y expo-cli. Con este proyecto aprendí a usar un poco de react js y el desarrollo multiplataforma en dispositivos mobiles.

## Descripción 📃
La aplciación está escrita en react native y expo, puede ejecutarce en dispositivos Android y iOS, usa firebase/firestore como base de de datos para almacenar y consultar toda la información que las aplicación que se ejecuta en dispositivos mobiles.

## Screnshots de la aplicación 📷
Aquí están algunas imagenes del proyecto en ejecución desde un dispositivo Android.
<div align="center">
<img height="500px" src="https://firebasestorage.googleapis.com/v0/b/crud-74894.appspot.com/o/Screenshot_20220620_022813_com.a329549.test.jpg?alt=media&token=8ba17675-f2b5-4312-bc19-9e541804f7fe" />
    <img height="500px" src="https://firebasestorage.googleapis.com/v0/b/crud-74894.appspot.com/o/Screenshot_20220620_022843_com.a329549.test.jpg?alt=media&token=3a0b8f40-9c41-41f0-ab40-8527ca1fe38b" />
    <img height="500px" src="https://firebasestorage.googleapis.com/v0/b/crud-74894.appspot.com/o/Screenshot_20220620_022854_com.a329549.test.jpg?alt=media&token=540351e3-ca13-46f2-80bc-e016a8264980"/>
</div>

## Instalación 🧱
Para ejecutar el proyecto en tu máquina, necesitas ejecutar los siguientes comandos.
```
$ git clone https://gitlab.com/a329549/food-sale.git
$ cd ./food-sale
$ npm install
```
Después hay que crear un archivo ".env" en la raíz del proyecto y colocar ahí las llaves del proyecto de firebase.
```
API_KEY=
AUTH_DOMAIN=
PROJECT_ID=
STORAGE_BUCKET=
MESSAGING_SENDER_ID=
APP_ID=
```
Por último, el siguiente comando correra la aplicación.
```
$ expo start
```
- Si tenemos un dispositivo android conectado solo hay que pulsar "a" y esperar a que la aplicación se muestre en él.
- También pudemos descargar la aplicación de Expo en Play store o App store para escanear el código QR que nos aparecera al correr el comando "expo start", y entonces la aplicación se mostrara en nuestro dispositivo mobil.

## Licencia 🎨
MIT
